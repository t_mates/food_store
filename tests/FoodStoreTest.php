<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use FoodStore\FoodStore;
use FoodStore\PreparedFoodsList;
use FoodStore\Entity\Food;
use FoodStore\Entity\Order;
use FoodStore\Entity\Payload;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Dotenv\Dotenv;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

final class FoodStoreTest extends TestCase
{
    protected $entityManager;

    /**
     * Загружает список продуктов питания в тестовую бд
     * перед каждым тестом.
     */
    protected function setUp(): void
    {
        require_once dirname(__DIR__, 1) . '/vendor/autoload.php';
        $dotenv = new Dotenv();
        $dotenv->load(dirname(__DIR__, 1) .'/.env_test');

        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration(
            [dirname(__DIR__, 1)."/src/Entity"],
            $isDevMode
        );
        // database configuration parameters
        $conn = array(
            'driver' => getenv('DB_DRIVER'),
            'host' => getenv('DB_HOST'),
            'dbname' => getenv('DB_NAME'),
            'user' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
        );
        // obtaining the entity manager
        $this->entityManager = EntityManager::create($conn, $config);
        foreach (PreparedFoodsList::get() as $foodName) {
            $food = new Food();
            $food->setName($foodName);
            $this->entityManager->persist($food);
        }
        $this->entityManager->flush();
    }

    /**
     * Проверяем что заказы создаются (по ответу)
     */
    public function testCanCreateOrders(): void
    {
        $createOrderJson = json_encode([
            'foods' => [
                [
                    'id' => '4',
                    'quantity' => '15'
                ],
                [
                    'id' => '1',
                    'quantity' => '2'
                ],
            ],
            'delivery_address' => 'Метро московская',
            'delivery_date' => '2019-04-23',
        ]);
        $createOrderRequest = Request::create('/api/v1/orders', 'POST', [], [], [], [], $createOrderJson);
        $app = new FoodStore($createOrderRequest, '.env_test');
        $app->processRequest();
        $response = $app->getResponse();
        $this->assertEquals(
            Response::HTTP_CREATED,
            $response->getStatusCode()
        );
        $this->assertTrue(isset(json_decode($response->getContent(), true)['orderId']));
    }

    /**
     * Проверям что количество созданных заказов соответствует созданным,
     * и что получение списка заказов работает
     */
    public function testCanShowListOfOrders(): void
    {
        $createOrderJson = json_encode([
            'foods' => [
                [
                    'id' => '1',
                    'quantity' => '10'
                ],
            ],
            'delivery_address' => 'Метро петроградская',
            'delivery_date' => '2019-04-21',
        ]);
        $createOrderRequest = Request::create('/api/v1/orders', 'POST', [], [], [], [], $createOrderJson);
        $app = new FoodStore($createOrderRequest, '.env_test');
        $app->processRequest();
        $showListRequest = Request::create(
            '/api/v1/orders',
            'GET'
        );
        $app->setRequest($showListRequest)->processRequest();
        $response = $app->getResponse();
        $this->assertEquals(
            1,
            count(json_decode($response->getContent(), true))
        );
        $this->assertEquals(
            Response::HTTP_OK,
            $response->getStatusCode()
        );
    }

    /**
     * После тестов очищаем все таблицы, обнуляем счетчики (только для postgres)
     */
    public function tearDown(): void
    {
        $query = $this->entityManager->createQuery('DELETE FROM FoodStore\Entity\Payload');
        $query->execute();
        $query = $this->entityManager->createQuery('DELETE FROM FoodStore\Entity\Order');
        $query->execute();
        $query = $this->entityManager->createQuery('DELETE FROM FoodStore\Entity\Food');
        $query->execute();
        $connection = $this->entityManager->getConnection();
        $connection->prepare('ALTER SEQUENCE foods_id_seq RESTART;')->execute();
        $connection->prepare('ALTER SEQUENCE orders_id_seq RESTART;')->execute();
        $this->entityManager->flush();
    }
}
<?php

use FoodStore\FoodStore;
use Symfony\Component\HttpFoundation\Request;

require_once dirname(__DIR__, 1) . '/vendor/autoload.php';

$app = new FoodStore(Request::createFromGlobals(), '.env');
$app->processRequest()->getResponse()->send();

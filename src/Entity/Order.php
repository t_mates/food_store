<?php

namespace FoodStore\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FoodStore\Entity\Payload;

/**
 * @Entity @Table(name="orders")
 **/
class Order
{
    const STATUS_NEW = 'new';
    const STATUS_PAYED = 'payed';

    /** 
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;
    /** 
     * @Column(type="string")
     */
    protected $status;
    /** 
     * @Column(type="string", name="delivery_address")
     */
    protected $deliveryAddress;
    /** 
     * @Column(type="date", name="delivery_date")
     */
    protected $deliveryDate;
    /** 
     * @OneToMany(targetEntity="Payload", mappedBy="order")
     */
    protected $payloads;

    public function __construct()
    {
        $this->payloads = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getPayloads()
    {
        return $this->payloads;
    }

    public function addPayload(Payload $payload)
    {
        $this->payloads[] = $payload;
        return $this;
    }

    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
        return $this;
    }

    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
        return $this;
    }
}
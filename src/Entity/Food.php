<?php

namespace FoodStore\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FoodStore\Entity\Payload;

/**
 * @Entity @Table(name="foods")
 **/
class Food
{
    /** 
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;
    /** 
     * @Column(type="string")
     */
    protected $name;
    /** 
     * @OneToMany(targetEntity="Payload", mappedBy="food")
     */
    protected $payloads;

    public function __construct()
    {
        $this->payloads = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getPayloads()
    {
        return $this->payloads;
    }

    public function addPayload(Payload $payload)
    {
        $this->payloads[] = $payload;
        return $this;
    }
}
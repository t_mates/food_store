<?php

namespace FoodStore;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Dotenv\Dotenv;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use FoodStore\Entity\Order;
use FoodStore\Controller\Order\ListOrdersController;
use FoodStore\Controller\Order\ShowOrderController;
use FoodStore\Controller\Order\CreateOrderController;
use FoodStore\Controller\Order\UpdateOrderController;
use FoodStore\Error\ValidationError;
use Throwable;

/**
 * Основной класс приложения
 */
class FoodStore
{
    const API_VERSION_V1 = 'v1';

    const APP_MODE_DEVELOPMENT = 'development';
    const APP_MODE_PRODUCTION = 'production';

    /**
     * Путь к корню сайта приложения, от него зависит где оно
     * будет искать конфиг и все остальное
     * @var string
     */
    protected $baseDir;
    /**
     * Менеджер сущностей
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * Объект запроса
     * @var Request
     */
    protected $request;
    /**
     * Объект ответа
     * @var Response
     */
    protected $response;
    /**
     * Коллекция роутов
     * @var RouteCollection
     */
    protected $routes;
    /**
     * Контекст запроса
     * @var RequestContext
     */
    protected $context;
    /**
     * Сопоставитель url
     * @var UrlMatcher
     */
    protected $matcher;

    /**
     * В конструктор надо передать запрос типа Response, имя файла окружения
     * (может быть несколько для разных окружений), и путь к корню приложения.
     * Если путь не передан, то приложение будет считать корнем директорию,
     * находящуюся на одну выше чем этот класс
     * @param Request     $request
     * @param string      $envFileName
     * @param string|null $baseDir
     */
    public function __construct(
        Request $request,
        string  $envFileName,
        string  $baseDir = null
    )
    {
        $this->setBaseDir($baseDir);
        $this->loadEnv($envFileName);
        $this->loadEntityManager(getenv('APP_MODE'));
        $this->assignRoutes($request);
        $this->request = $request;
    }

    /**
     * Получает ответ, может быть null если не был вызван метод processRequest()
     * или может содержать данные предыдущего запроса если processRequest() не
     * был вызван повторно после загрузки нового Request
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * Сам процесс обрабоки запроса - в this->request сохраняется результат
     * работы. Возвращает себя
     * @return FoodStore
     */
    public function processRequest(): FoodStore
    {
        try {
            $parameters = $this->matcher->match($this->context->getPathInfo());
            $controller = new $parameters['_controller'];
            $this->response = $controller->hanlde(
                $this->request,
                $this->entityManager,
                $parameters
            );
            return $this;
        } catch (ValidationError $e) {
            $this->response = new Response(
                json_encode(['error' => $e->getMessage()]),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                ['content-type' => 'application/json']
            );
            return $this;
        } catch (ResourceNotFoundException $e) {
            $this->response = new Response(
                json_encode(['error' => $e->getMessage()]),
                Response::HTTP_NOT_FOUND,
                ['content-type' => 'application/json']
            );
            return $this;
        } catch (Throwable $t) {
            $this->response = new Response(
                json_encode(['error' => $t->getMessage()]),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                ['content-type' => 'application/json']
            );
            return $this;
        }
    }

    /**
     * Позволяет установить новый Request в приложение, что бы получить новый
     * результат. Это нужно в тестах, что бы делать несколько запросов не 
     * создавая заново экземпляры приложения
     * @param Request $request
     * @return FoodStore
     */
    public function setRequest(Request $request): FoodStore
    {
        $this->assignRoutes($request);
        $this->request = $request;
        return $this;
    }

    /**
     * Устанавливает корневую папку для приложения
     * @param string $path
     * @return FoodStore
     */
    protected function setBaseDir(string $path): FoodStore
    {
        if (is_null($path)) {
            $this->baseDir = dirname(__DIR__, 1);
        } else {
            $this->baseDir = $path;
        }
        return $this;
    }

    /**
     * Загружает конфиг окружения (пароль от бд и прочее)
     * @param  string $envFilePath
     * @return FoodStore
     * @return FoodStore
     */
    protected function loadEnv(string $envFilePath): FoodStore
    {
        $dotenv = new Dotenv();
        $dotenv->load($this->baseDir . '/' . $envFilePath);
        return $this;
    }

    /**
     * Загружает менеджер сущностей и принимает параметр из окружения - режим
     * работы, development или production
     * @param  string $appMode 
     * @return FoodStore
     */
    protected function loadEntityManager(string $appMode): FoodStore
    {
        // Create a simple "default" Doctrine ORM configuration for Annotations
        $isDevMode = $appMode === self::APP_MODE_DEVELOPMENT;
        $config = Setup::createAnnotationMetadataConfiguration(
            [$this->baseDir."/src/Entity"],
            $isDevMode
        );
        // database configuration parameters
        $conn = array(
            'driver' => getenv('DB_DRIVER'),
            'host' => getenv('DB_HOST'),
            'dbname' => getenv('DB_NAME'),
            'user' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
        );
        // obtaining the entity manager
        $this->entityManager = EntityManager::create($conn, $config);
        return $this;
    }

    /**
     * Загружает роуты, обозначенные ниже, в текущий RouteCollection, и также
     * загружает коллекцию роутов в UrlMatcher, который после этого готов 
     * проверять совпадения
     * @param  Request $request
     * @return FoodStore
     */
    protected function assignRoutes(Request $request): FoodStore
    {
        $this->routes = new RouteCollection();
        foreach ($this->getRoutes() as $routeName => $route) {
            $this->routes->add($routeName, $route);
        }
        $this->routes->addPrefix('/api/'.self::API_VERSION_V1);
        $this->context = new RequestContext();
        $this->context->fromRequest($request);
        $this->matcher = new UrlMatcher($this->routes, $this->context);
        return $this;
    }

    /**
     * Просто возвращает список роутов для приложения, которым назначены
     * контроллеры. Каждый элемент рассчитан на передачу его в класс 
     * RouteCollection
     * @return array
     */
    protected function getRoutes(): array
    {
        return [
            'list_orders' => new Route(
                '/orders',
                ['_controller' => ListOrdersController::class],
                [],
                [],
                null,
                [],
                ['GET']
            ),
            'create_order' => new Route(
                '/orders',
                ['_controller' => CreateOrderController::class],
                [],
                [],
                null,
                [],
                ['POST']
            ),
            'show_order' => new Route(
                '/orders/{orderId}',
                ['_controller' => ShowOrderController::class],
                [],
                [],
                null,
                [],
                ['GET']
            ),
            'update_order' => new Route(
                '/orders/{orderId}',
                ['_controller' => UpdateOrderController::class],
                [],
                [],
                null,
                [],
                ['PATCH']
            ),
        ];
    }
}
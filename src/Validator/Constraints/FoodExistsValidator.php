<?php

namespace FoodStore\Validator\Constraints;

use FoodStore\Validator\Constraints\FoodExists;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use FoodStore\Entity\Food;

class FoodExistsValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof FoodExists) {
            throw new UnexpectedTypeException($constraint, FoodExists::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_numeric($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'string');

            // separate multiple types using pipes
            // throw new UnexpectedValueException($value, 'string|int');
        }

        // validaton itself
        $food = $constraint->entityManager->find(Food::class, $value);
        if (is_null($food)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ foodId }}', $value)
                ->addViolation();
        }
    }
}
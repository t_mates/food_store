<?php

namespace FoodStore\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Doctrine\ORM\EntityManager;

/**
 * @Annotation
 */
class OrderExists extends Constraint
{
    public $message = 'This order id "{{ orderId }}" is not exists.';
    public $entityManager;

    public function __construct($options, EntityManager $entityManager)
    {
        parent::__construct($options);
        $this->entityManager = $entityManager;
    }
}

<?php

namespace FoodStore\Controller\Order;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use FoodStore\Entity\Order;
use FoodStore\Entity\Food;
use FoodStore\Entity\Payload;

class ListOrdersController
{
    /**
     * Основной метод контроллера
     * @param  Request       $request
     * @param  EntityManager $entityManager
     * @param  array         $parameters    Параметры из uri и прочее
     * @return Response
     */
    public function hanlde(
        Request       $request,
        EntityManager $entityManager,
        array         $parameters
    ): Response
    {
        $query = $entityManager->createQuery('
            SELECT o, p, f 
            FROM FoodStore\Entity\Order o 
            JOIN o.payloads p 
            JOIN p.food f'
        );
        $orders = $query->getResult();
        $data = [];
        foreach ($orders as $order) {
            $data[] = [
                'orderId' => $order->getId(),
                'status' => $order->getStatus(),
                'foods' => $order->getPayloads()->map(function($payload) {
                    return [
                        'food_name' => $payload->getFood()->getName(),
                        'quantity' => $payload->getQuantity(),
                    ];
                })->toArray(),
            ];
        }
        return new Response(
            json_encode($data),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
    }
}